require 'json'
require 'byebug'

Record = Struct.new(:error_type, :size, :memsize, :loc)

class RecordMap
  def initialize
    @hash = {}
  end

  def <<(record)
    key = get_key(record)
    @hash[key] ||= []
    @hash[key] << record
  end

  def each(&blk)
    @hash
    .sort_by { |(err_type, loc), records| [err_type, loc] }
    .each do |(err_type, loc), records|
      yield err_type, loc, records
    end
  end

  private

  def get_key(record)
    [record.error_type, record.loc]
  end
end

map = RecordMap.new
File.open('/tmp/validation_errors.ndjson', 'r').each_line do |l|
  parsed = JSON.parse(l)
  obj = Record.new(parsed['type_of_validation_error'], parsed['size'], parsed['memsize_kb'], parsed['location_in_schema'])
  next unless obj.error_type == 'maxItems'
  map << obj
end

def percentile(arr, percentile)
  k = (percentile*(arr.length-1)+1).floor - 1
  f = (percentile*(arr.length-1)+1).modulo(1)
  arr[k] + (f * (arr[k+1] - arr[k]))
end

last_loc=nil
map.each do |err, loc, records|
  if last_loc != loc
    puts unless loc.nil?
    puts "error type: #{err} (schema location: #{loc})"
    last_loc = loc
  end
  sizes = records.map { |r| r.size }.sort
  [0.5, 0.75, 0.95, 0.99, 0.999].each do |p|
    puts "percentile #{p}, value: #{percentile(sizes, p)}"
  end
end
