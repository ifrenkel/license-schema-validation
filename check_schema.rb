require 'open-uri'
require 'json'
require 'json_schemer'
require 'byebug'
require 'objspace'

branch = ARGV[0] ? ARGV[0] : 'master'

schema_url = "https://gitlab.com/gitlab-org/gitlab/-/raw/#{branch}/ee/app/validators/json_schemas/pm_package_licenses.json"
schema_path = File.join('/tmp', 'pm_package_licenses.json')
URI.open(schema_url) do |f|
  File.open(schema_path, 'w') do |tofile|
    IO.copy_stream(f, tofile)
  end
end
schema = JSONSchemer.schema(Pathname.new(schema_path))

urls = [
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/conan_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/go_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/maven_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/npm_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/nuget_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/packagist_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/pypi_licenses_v2.ndjson',
  'https://storage.googleapis.com/dev-export-license-bucket-60cbcc4228890ff5/v2_preview/rubygem_licenses_v2.ndjson'
]

ndjson_files=[]
urls.each do |url|
  path = File.join('/tmp', url.split('/').last)
  ndjson_files << path

  next if File.exists?(path)
  puts "downloading: #{url}"

  URI.open(url) do |f|
    IO.copy_stream(f, File.open(path, 'w'))
  end
end

# get a unique int for each license name to simulate spdx_identifier to license
# id translation during ingestion 
def licdb_get(license)
  @licdb ||= {}
  unless @licdb.has_key?(license)
    @id ||= 0
    @id += 1
    @licdb[license] = @id
  end
  @licdb[license]
end

errors = {}
nfiles = 0
nlines = 0
nerrs = 0
ndjson_files.each do |path|
  nfiles += 1
  File.open(path, 'r').each_line do |line|
    nlines += 1
    obj = JSON.parse(line)
    name = obj['name']
    purl_type = path.split('/').last.split('_').first
    licenses = [
      obj['default_licenses'].map { |license| licdb_get(license) },
      obj['lowest_version'],
      obj['highest_version'],
      obj['other_licenses'].map { |hash| [hash['licenses'].map { |license| licdb_get(license) }, hash['versions']] } 
    ]
    errs = schema.validate(licenses)
    errs.each do |err|
      nerrs += 1

      # the data pointer shows location of the offending data via a string like
      # /3/0/1
      # we can turn that into an array like [3,0,1] and then dereference the
      # licenses arr via licenses.dig(3,0,1)
      elt_idx_path = err['data_pointer'].delete_prefix('/').split('/').map(&:to_i)
      elt_data = licenses.dig(*elt_idx_path) || []

      errors[err['type']] ||= {}
      errors[err['type']][err['schema_pointer']] ||= []
      memsize = ObjectSpace.memsize_of(elt_data)/1024.0
      errors[err['type']][err['schema_pointer']] << {'where' => err['data_pointer'], 'size' => elt_data.size, 'purl_type' => purl_type, 'name' => name, 'licenses' => licenses, 'memsize_kb' => memsize}
    end
    print "files processed: #{nfiles}, lines processed: #{nlines}, errors:: #{nerrs}\r"
  end
end

# write to an errors file as ndjson
# derefrencing by key allows us to group errors by type and where in the schema
# they are for faster processing
errfile_path = '/tmp/validation_errors.ndjson'
File.open(errfile_path, 'w') do |f|
  errors.each do |errtype, loc_errs|
    loc_errs.each do |loc, errs|
      errs.each do |err|
        res = { purl_type: err['purl_type'], name: err['name'], type_of_validation_error: errtype, location_in_schema: loc, where_in_data: err['where'], size: err['size'], memsize_kb: err['memsize_kb'] }
        f.write("#{JSON.dump(res)}\n")
      end
    end
  end
end

puts "wrote error list to #{errfile_path}"
