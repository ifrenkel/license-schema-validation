require 'json'
require 'byebug'

LARGEST_BUCKET = 2_000
BUCKETS = (0..LARGEST_BUCKET).step(50).to_a
BUCKET_NAMES = BUCKETS.each_with_index.map do |bkt, idx|
  if idx == 0
    name = "0..#{BUCKETS[idx+1]}"
  else
    name = "#{BUCKETS[idx]}..#{BUCKETS[idx+1]}"
  end
  [bkt, name]
end.to_h

Record = Struct.new(:error_type, :size, :memsize, :loc)

class Histogram
  def initialize
    @hash = {}
  end

  def <<(record)
    key = get_key(record)
    @hash[key] ||= []
    @hash[key] << record
  end

  def each(&blk)
    @hash
    .sort_by { |(err_type, loc, bucket), records| [err_type, loc, bucket] }
    .each do |(err_type, loc, bucket), records|
      yield err_type, loc, bucket, records.size
    end
  rescue => e
    byebug
  end

  private

  def get_key(record)
    [record.error_type, record.loc, bucket(record)]
  end

  def bucket(record)
    res = -1
    BUCKETS.each do |bkt|
      break if record.size < bkt
      res = bkt
    end
    res
  end
end

counter= Histogram.new
File.open('/tmp/validation_errors.ndjson', 'r').each_line do |l|
  parsed = JSON.parse(l)
  obj = Record.new(parsed['type_of_validation_error'], parsed['size'], parsed['memsize_kb'], parsed['location_in_schema'])
  counter << obj
end

last_loc=nil
counter.each do |err, loc, bkt, size|
  if last_loc != loc
    puts unless loc.nil?
    puts "error type: #{err} (schema location: #{loc})"
    last_loc = loc
  end
  next if size == 0
  mult = (size/50) + 1
  name = BUCKET_NAMES[bkt]
  puts "#{name}: #{'*'*mult} (#{size})"
end
