require 'json'
require 'byebug'

Record = Struct.new(:purl_type, :error_type, :size, :memsize, :loc)

class Counter
  def initialize
    @hash = {}
  end

  def <<(record)
    @hash[record.purl_type] ||= {}
    @hash[record.purl_type][record.error_type] ||= {}
    @hash[record.purl_type][record.error_type][record.loc] ||= { count: 0, max_val: 0, memsizes: [], max_memsize: 0 }
    @hash[record.purl_type][record.error_type][record.loc][:count] += 1
    if @hash[record.purl_type][record.error_type][record.loc][:max_val] < record.size
      @hash[record.purl_type][record.error_type][record.loc][:max_val] = record.size
      @hash[record.purl_type][record.error_type][record.loc][:memsizes] << record.memsize
    end
    if @hash[record.purl_type][record.error_type][record.loc][:max_memsize] < record.memsize
      @hash[record.purl_type][record.error_type][record.loc][:max_memsize] = record.memsize
    end
  end

  def each(&blk)
    @hash.each do |purl_type, errors|
      errors.each do |err_type, records|
        records.each do |loc, stat|
          total_memsize = stat[:memsizes].inject(&:+)
          avg_memsize = total_memsize/stat[:memsizes].size
          yield purl_type, err_type, loc, stat[:count], stat[:max_val], avg_memsize, stat[:max_memsize], total_memsize
        end
      end
    end
  end
end

counter= Counter.new
File.open('/tmp/validation_errors.ndjson', 'r').each_line do |l|
  parsed = JSON.parse(l)
  obj = Record.new(parsed['purl_type'], parsed['type_of_validation_error'], parsed['size'], parsed['memsize_kb'], parsed['location_in_schema'])
  counter << obj
end

@col_width=20

def print_fields(*fields)
  puts fields.map { |field| field.to_s.center(@col_width) }.join(' | ')
end

columns = ['purl type', 'err type', 'num times seen', 'max val', 'avg memsize (kb)', 'max mem size (kb)', 'total mem size (kb)', 'location in schema']
print_fields(*columns)
puts('-'*((columns.size*20)+(columns.size*3)-3))
counter.each do |purl_type, err_type, schema_loc, err_count, max_num_items, avg_mem, max_mem, total_mem|
  print_fields(purl_type, err_type, err_count, max_num_items, sprintf('%.2f', avg_mem), sprintf('%.2f', max_mem), sprintf('%.2f', total_mem), schema_loc)
end
