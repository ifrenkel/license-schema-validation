# license-schema-validation

This project is used to validate version format 2 ndjson export files against
monolith's schema.

## Install

```ruby
bundle install
```

## Generate validation error list

To validate all files run:

```ruby
bundle exec ruby check_schema.rb [branch]
```

### Schema

The monolith schema is downloaded from: "https://gitlab.com/gitlab-org/gitlab/-/raw/#{branch}/ee/app/validators/json_schemas/pm_package_licenses.json"

Where `branch` is `master` by default.

### Data files

The exported data file urls are hard-coded in `check_schema.rb`.

### Output

The output is an `ndjson` file which is a list of packages that generated
validation errors and is written `/tmp/validation_errors.ndjson`.

Fields output:
* `purl_type`
* `name`: package name
* `type_of_validation_error`: the type reported by JSONSchemer (e.g.
  `maxItems`)
* `location_in_schema`: on which schema element this error appeared
  (e.g. `default_licenses`)
* `where_in_data`: index path in the data array (e.g. `/3/0/1` points to `other_license`, `first
  tuple`, `versions list`)
* `size`: the cardinality of the dataset (most often encountered is maxItem
  violation)
* `memsize_kb`: the size of the data structure in memory

## Analyze files

Analyze validation errors and print some common stats. Done by grouping the
errors by `purl type`, `error type`, and `schema location`. And then outputting stats such as
cardinality of offending element, size in memory, etc.

Run by `bundle exec ruby analyze_errors.rb`. The input error list is expected
to be at `/tmp/validation-errors.ndjson`.
